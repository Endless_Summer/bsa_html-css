FROM nginx:alpine
COPY nginx.conf /etc/nginx/nginx.conf
RUN rm -rf /usr/share/nginx/html/*

COPY ./src /usr/share/nginx/html/src
COPY ./index.html /usr/share/nginx/html/index.html

EXPOSE 80
ENTRYPOINT ["nginx", "-g", "daemon off;"]
